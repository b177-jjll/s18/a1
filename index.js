let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Mew', 'Venusaur', 'Jigglypuff'],
	friends: {
		hoenn: ['May','Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function() {
		console.log(this.pokemon[0] + '! I choose you!')
	}
}
console.log(trainer);

console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method:');
trainer.talk();

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target) {
		target.health -= this.attack;

		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + target.health + '.');

		if(target.health > 0) {
			console.log(target);
		}

		else {
			console.log(target.name + this.faint());
			console.log(target);
		}
	};
	this.faint = function() {
		return ' fainted.';
	};

}

let pikachu = new Pokemon('Pikachu', 50);
	mew = new Pokemon('Mew',100);
	rapidash = new Pokemon('Rapidash', 30);
	eevee = new Pokemon('Eevee', 40,);

console.log(pikachu);
console.log(mew);
console.log(eevee);

eevee.tackle(pikachu);
mew.tackle(eevee);